# Report

## Program Flow
At the highest level, our shell is simply an infinite while loop that:
 1. Reads in the user's command into a string. `prompt_user()`
 2. Breaks up that string into individual tokens. `tokenize_command()`
 3. Performs error checking to ensure that the user's input was valid. 
 `has_errors()` and `obj_has_errors()`
 4. Creates an object representation of the string that was read from the user.
 `create_cmds_obj()` 
 5. Executes the command. `exec_commands()`

## Files
Our program's functions are separated into four files, which improved code 
modularity, code reusability and our workflow in general. `cmd_parser.c` manages
parsing, `cmd_validator.c` manages error checking, `cmd_obj.c` manages object 
creation, and `cmd_executor.c` manages command execution.

## Reading Commands
In `prompt_user()`, we wait for the user to enter input, and we use `fgets()`
to read in 512 characters into `user_input`. We chose `fgets()` because it is
 very convenient for reading input until a newline character and does not 
 require much effort from our end.

We proceed with `tokenize_command()` which breaks up `user_input` into tokens 
and stores them in`tok_cmd`.  Originally, we tried using `strtok()`, but 
`strtok()` removes delimiter characters such as `<` and `>`. We wanted to keep
these characters, so we decided to implemented our own version of `strtok()`, 
which made things a lot easier. In our function, we loop through `user_input`, 
character by character, until we find any special characters. Once we do, we 
take the chunk of characters that came before that special character and copy it
 into `tok_cmd`. We repeat this process for the entire string, while also 
 inserting the special characters. 

Also, note, we decided to copy over the entire string instead of having pointers
 to the original string. We do this because we need to save the original 
 unmodified string for later when we print the completion message.

## Error Checking
Now, we're ready for the error checking phase. We decided to combine almost all 
of the error checking in one place, because it makes it easier to modify and add
more error checking later on if we need to. There are two main functions that 
perform error checking `has_errors()` and `obj_has_errors()`. These technically
could have been combined into one function, but some errors were significantly
easier to process once we created an object representation of the command, so
we decided to separate them from the more urgent error checking. 

In `has_errors()`, we loop through each string in `tok_cmd` and check only the 
first character of each string to find the special character. We're making the 
assumption that our tokenizer processed the string correctly, so it is 
sufficient to only check the first character of each string when searching for 
special characters.

## Data Structure
Finally, once we passed all of our checks, we can be sure that the user's 
command will fit the mold of our data structure, so we construct it in 
`create_cmds_obj()`. Our data structure is simply a large struct that contains 
all the fields that we would ever need to know when executing the command. This 
includes:

 - `char *prog_name` the program name, the 1st parameter of `execvp()`
 - `char **tok_args` program arguments, the 2nd parameter of `execvp()`
 - `char *cmd_str` a string containing exactly what the user entered
 - `int num_args` the number of arguments for the command
 - `char *input` where the command will be getting its input from
 - `char *output`where the command will be outputting to 
 - `int status` the exit status of the command
 - `int pid` the pid of the command
 - `bool bg` whether or not the command needs to run in the background

We combine this struct into an array to form a piped command. To save time on 
implementation, we created a fixed size of 256 command pointers, so we wouldn't 
have to worry about reallocating the amount of pointers we would need, since we 
know 256 commands is the upper limit. These pointers do not take up much memory,
so implementing it this way does not cause any major concerns.

## Command Execution
Now that we created the object, we can execute that command in the function 
`exec_commands()`. We have code that is very similar to what was presented in 
lecture. The process `forks()`, the child executes the command, while the parent
waits.
 
## Built-In Commands
In the function `exec_commands()`, we also process the built in commands. These 
run on the parent process. Originally, for some reason, we thought these 
commands had to run on the child process like the other commands, but we 
realized that didn't make any sense because we wouldn't be able to do basic 
things like kill the parent process. Therefore, before we fork, we use 
`strcmp()` to check if the command is a built-in command and if it is, we skip 
the usual forking process and execute that command on the parent process.

## Input and Output Redirection
Input and output redirection was straightforward. We have a function 
`change_fds()`, which opens a file and duplicates it into either `stdin` or 
`stdout` with the proper permissions. This section did not require much thought 
when implementing it.

## Pipeline Commands
For piped commands, we decided to have the parent create all the commands at 
once instead of nesting the piped commands. This made it relatively 
straightforward to implement, so we didn't have to use recursion or anything 
complex like in our initial attempts. (In lecture, we only saw how to pipe two
commands, so it was a bit tricky to figure out how that idea extended beyond
two commands.) We used a 'for' loop for the parent and created and executed all
the child processes at once, so we don't have to worry about buffer overflow in
the pipes. 

Before the child executes the process, we assign the proper pipe indices with 
`change_fds()`. This function assigns the correct `piped()` `stdin`/`stdout` 
locations to the chain of programs that are executing. We decided to create 
all the pipe file descriptors at once. Once have all the pipes set up, it was a 
bit tricky figure out the correct indices that we needed to access for each 
pipe. However, with some math and sequence analysis, we found that the 
$2 * (i - 1)$th index gives the `stdin` location and the $2* i + 1$th location 
gives us the stdout location for the set of consecutive pipe file descriptor 
array that we created.

## Background Commands
This was the most tricky to figure out. The entire time before this phase, our 
parent was waiting for the child to finish, but now we needed to somehow make 
it not wait, but at the same time, wait so that print out the completion message
when it is finished. The secret to our implementation was calling `fork()` a 
second time in `main()`,  if we were processing a background command. This way,
we can continue to execute the command on a separate child process, while the 
parent can proceed and display the shell prompt. Otherwise, if we aren't 
attempting to execute a background process, we wait as usual. 

However, this still did not address the issue of the shell exiting prematurely 
when the child process is still executing a program in the background. So our 
solution to this problem was to check if the parent had any children process 
running using the `wait()` function with the `WNOHANG` option (so that it 
doesn't have to wait for the process to end), and if it has a child process, 
then prevent the shell from exiting.

## Testing
Our testing first consisted of passing the tests in the "test_sshell_student.sh" 
script. Once we were able to do this, we tested several of our own commands on 
the executable file that we were provided with to discover edge cases, 
especially when trying to figure out how the error checking worked. We typically
saved the output into a file using `./sshell 2>&1 > ours.txt` and then using `diff`
to compare the results.

Finally, we spent a lot of time using `GDB` to debug our code at every step of
the way when we ran into errors. We compiled with the `-g` option which helped
find which lines in our code were responsible for the segfaults that we were
getting. We also used `Valgrind` to make sure that our program did not have 
any memory leaks because those errors also caused some odd runtime behaviors.

## Sources used
- In-class Lecture Notes
- [Linux man pages](https://linux.die.net/man/)
- [The GNU C 
library](https://www.gnu.org/software/libc/manual/html_mono/libc.html)
- [Implementation of multiple pipes in C - 
StackOverflow](https://stackoverflow.com/questions/8389033/)
- [Using waitpid to run process in background? - 
StackOverflow](https://stackoverflow.com/questions/14548367/)