#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>


#include "cmd_parser.h"


//  Input: 
//          char *cmd_str, an uninitialized string
//  Purpose:
//          displays "sshell$" prompt
//          reads in whatever the user enters at prompt into cmd_str
//
void prompt_user(char *cmd_str)
{
    printf("sshell$ ");
    fgets(cmd_str, 512, stdin);

    int cmd_len = strlen(cmd_str);

    // used for grading script
    if (!isatty(STDIN_FILENO)) {
        printf("%s", cmd_str);
        fflush(stdout);
    }

    // replaces '\n' with '\0'
    if (cmd_str[cmd_len - 1] == '\n')
        cmd_str[cmd_len - 1] = '\0';
}


//  Input: 
//          char *cmd_str, user's input from shell prompt
//          char **tok_cmd, uninitialized string
//  Purpose:
//          breaks up 'cmd_str' into individual tokens
//          i.e. "echo hello > file.txt" becomes 
//          {"echo", "hello", ">", "file.txt"}
//
void tokenize_command(char *cmd_str, char **tok_cmd)
{
    int cmd_len = strlen(cmd_str);
    int num_tok = 0;
    int start_index = 0;

    for(int i = 0; i <= cmd_len; i++) {
        int tok_size = i - start_index;

        if (cmd_str[i] == ' ' || is_special_token(cmd_str[i]) || cmd_str[i] == '\0') {
            // adds token that came before a
            // space, special token or end of string
            if (tok_size > 0) {
                tok_cmd[num_tok] = malloc(tok_size * sizeof(char) + 1);
                strncpy(tok_cmd[num_tok], &cmd_str[start_index], tok_size);
                tok_cmd[num_tok++][tok_size] = '\0';
            }

            // adds '<' '>' '&' tokens instead of skipping over them 
            // like in white spaces
            if (is_special_token(cmd_str[i])) {
                tok_cmd[num_tok] = malloc(sizeof(char) + 1);
                strncpy(tok_cmd[num_tok], &cmd_str[i], 1);
                tok_cmd[num_tok++][1] = '\0';
            }

            skip_white_spaces(&i, cmd_str);
            start_index = i+1;
        }
    }

    tok_cmd[num_tok] = NULL;
}


//  Input: 
//          char *cmd_str, user's input from shell prompt
//          int *i, index for cmd_str (used in conjuction with tokenize_cmd())
//  Purpose:
//          modifies int *i to the next available non-whitespace character
//          in char *cmd_str
//
void skip_white_spaces(int *i, char *cmd_str)
{
    *i += 1;

    while (cmd_str[*i] == ' ')
        *i += 1;

    *i -= 1;
}


//  Purpose:
//          returns true if a special character is detected
//
bool is_special_token(char cur_tok)
{
    if(cur_tok == '|' || cur_tok == '>' || cur_tok == '<' || cur_tok == '&')
        return true;

    return false;
}