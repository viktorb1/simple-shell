#ifndef _CMD_EXECUTOR_H_
#define _CMD_EXECUTOR_H_

#include <stdbool.h>

int exec_commands(struct Command **cmd,  int num_cmds);
void change_fds(struct Command **cmd, int i, int *fd, int num_cmds);
bool is_builtin(char *prog_name);
int exec_builtin(struct Command *cmd);
void close_fds(int *fd, int num_cmds);
void print_completion_msg(struct Command **cmd, int num_cmds);

#endif