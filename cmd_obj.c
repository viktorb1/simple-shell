#include <stdlib.h>

#include "cmd_obj.h"

//  Input: 
//          struct Command **cmd, array of commands
//          char **cmd_tok, tokenized version of char *cmd_str
//          char *cmd_str, original user's input string
//  Purpose:
//          converts char **cmd_tok into an object representation
//          returns number of commands detected (>1 if is a piped command)
//
int create_cmds_obj(struct Command **cmd, char **cmd_tok, char *cmd_str)
{
    int num_cmds = 0;
    int num_tok = get_num_tokens(cmd_tok);

    // extracts args, input, output, if bg process, etc. from tokenized string
    for(int i = 0; i < num_tok; i++)
    {
        cmd[num_cmds] = malloc(sizeof(struct Command));
        cmd[num_cmds]->num_args = 0;
        cmd[num_cmds]->cmd_str = cmd_str;
        cmd[num_cmds]->prog_name = cmd_tok[i];
        cmd[num_cmds]->tok_args = malloc((num_tok + 1) * sizeof(char*));
        cmd[num_cmds]->input = NULL;
        cmd[num_cmds]->output = NULL;
        cmd[num_cmds]->bg = false;

        // processes each token until '|' is reached or end of tokens
        while(i < num_tok && cmd_tok[i][0] != '|')
        {
            if(cmd_tok[i][0] == '<')
                cmd[num_cmds]->input = cmd_tok[++i];
            else if(cmd_tok[i][0] == '>')
                cmd[num_cmds]->output = cmd_tok[++i];
            else if(cmd_tok[i][0] == '&')
                cmd[0]->bg = true;
            else
                cmd[num_cmds]->tok_args[cmd[num_cmds]->num_args++] = cmd_tok[i];

            i++;
        }

        cmd[num_cmds]->tok_args[cmd[num_cmds]->num_args] = NULL;
        num_cmds++;
    }

    cmd[num_cmds] = NULL;
    return num_cmds;
}

// Purpose: 
//      returns number of tokens in char **cmd_tok
int get_num_tokens(char **cmd_tok)
{
    int i = 0;

    while(cmd_tok[i])
        i++;

    return i;
}


// Purpose:
//      frees malloc'ed object and object related data
void free_obj(struct Command **cmds, char **tok_cmd)
{
    for(int i = 0; tok_cmd[i]; i++)
        free(tok_cmd[i]);
        

    for(int i = 0; cmds[i]; i++) {
        free(cmds[i]->tok_args);
        free(cmds[i]);
    }
}