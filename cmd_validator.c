#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include "cmd_validator.h"
#include "cmd_parser.h"
#include "cmd_obj.h"


//  Input: 
//          char **tok_cmd, tokenized version of user's input from shell
//  Purpose:
//          performs error checking, checks if user input is valid
//          returns true if invalid, false if valid 
//
bool has_errors(char **tok_cmd)
{
    int num_tokens = get_num_tokens(tok_cmd);

    if (num_tokens == 0) // if string is empty
        return true;

    int first_tok = tok_cmd[0][0];


    if (is_special_token(first_tok)) { // if string starts with special char
        fprintf(stderr, "Error: invalid command line\n");
        return true;
    }

    // checks if two consecutive tokens are reserved characters
    for(int i = 1; i < num_tokens - 1; i++) {
        int cur_tok = tok_cmd[i][0];
        int next_tok = tok_cmd[i+1][0];

        if (cur_tok == '&') {
            fprintf(stderr, "Error: mislocated background sign\n");
            return true;
        } else if (cur_tok == '|') {
            if (is_special_token(next_tok)) {
                fprintf(stderr, "Error: invalid command line\n");
                return true;
            }
        } else if (cur_tok == '<') {
            if (is_special_token(next_tok)) {
                fprintf(stderr, "Error: no input file\n");
                return true;
            }
        } else if (cur_tok == '>') {
            if (is_special_token(next_tok)) {
                fprintf(stderr, "Error: no output file\n");
                return true;
            }
        }
    }

    // checks if last character is reserved character
    int last_tok = tok_cmd[num_tokens-1][0];

    if (last_tok == '|') {
        fprintf(stderr, "Error: invalid command line\n");
        return true;
    } else if (last_tok == '<') {
        fprintf(stderr, "Error: no input file\n");
        return true;
    } else if (last_tok == '>') {
        fprintf(stderr, "Error: no output file\n");
        return true;
    }

    return false;
}



//  Input: 
//          struct Command** cmd, an object representation of user input
//          int num_cmds, number of commands found in struct Command** cmd
//  Purpose:
//          -- performs even more error checking at a higher level
//          -- returns true object has errors, false if object is error-free
//          -- error checking at this level is much easier than at
//             the char **tok_cmd level
//
bool obj_has_errors(struct Command **cmd, int num_cmds)
{
    // only allow first command in pipe to have input redirection
    for(int i = 1; i < num_cmds; i++)
        if (cmd[i]->input) {
            fprintf(stderr, "Error: mislocated input redirection\n");
            return true;
        }

    // only allow last command in pipe to have output redirection
    for(int i = 0; i < num_cmds-1; i++)
        if (cmd[i]->output) {
            fprintf(stderr, "Error: mislocated output redirection\n");
            return true;
        }

    // only 16 arguments are allowed
    for(int i = 0; i < num_cmds; i++)
        if(cmd[i]->num_args > 16) {
            fprintf(stderr, "Error: too many process arguments\n");
            return true;
        }

    // check if input file is openable
    if(cmd[0]->input) {
        int fd = open(cmd[0]->input, O_RDONLY, 0444);

        if(fd == -1) {
            fprintf(stderr, "Error: cannot open input file\n");
            return true;
        }

        close(fd);
    }

    // check if output file is openable
    if(cmd[num_cmds-1]->output) {
        int fd = open(cmd[num_cmds-1]->output, O_RDWR | O_CREAT, 0644);
        
        if(fd == -1) {
            fprintf(stderr, "Error: cannot open output file\n");
            return true;
        }

        close(fd);
    }

    return false;
}