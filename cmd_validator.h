#ifndef _CMD_VALIDATOR_H_
#define _CMD_VALIDATOR_H_

#include <stdbool.h>
#include "cmd_obj.h"

bool has_errors(char **tok_cmd);
bool obj_has_errors(struct Command **cmd, int num_cmds);

#endif