#ifndef _CMD_PROCESSING_H_
#define _CMD_PROCESSING_H_

void prompt_user(char *cmd_str);
void tokenize_command(char *cmd_str, char **tok_cmd);
bool has_errors(char **tok_cmd);
bool is_special_token(char cur_tok);
void skip_white_spaces(int *i, char *cmd_str);

#endif