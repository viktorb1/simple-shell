#ifndef _CMD_OBJ_H_
#define _CMD_OBJ_H_

#include <stdbool.h>

struct Command {
    char *prog_name;
    char **tok_args;
    char *cmd_str;
    int num_args;

    char *input;
    char *output;

    int status;
    int pid;
    bool bg;
};

int create_cmds_obj(struct Command **cmd, char **cmd_tok, char *cmd_str);
int get_num_tokens(char **cmd_tok);
void free_obj(struct Command **cmds, char **tok_cmd);

#endif