#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

#include "cmd_parser.h"
#include "cmd_obj.h"
#include "cmd_validator.h"
#include "cmd_executor.h"

int main(int argc, char *argv[])
{
    while(1) {
        struct Command *cmds[256];
        char user_input[512];
        char *tok_cmd[256];

        prompt_user(user_input);
        tokenize_command(user_input, tok_cmd);

        if(!has_errors(tok_cmd)) {
            int num_cmds = create_cmds_obj(cmds, tok_cmd, user_input);

            if (!obj_has_errors(cmds, num_cmds)) {
                // if command should run in the background
                if (cmds[0]->bg) {
                    if(fork() == 0) {
                        exec_commands(cmds, num_cmds);
                        exit(EXIT_SUCCESS);
                    }
                } else // command should run in the foreground
                    exec_commands(cmds, num_cmds);

                free_obj(cmds, tok_cmd);
            }
        }
    }

    return EXIT_SUCCESS;
}
