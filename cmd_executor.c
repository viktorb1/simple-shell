#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <string.h>
#include <fcntl.h>

#include "cmd_obj.h"
#include "cmd_executor.h"

//  Input: 
//          struct Command **cmd, contains list of piped commands (or one cmd)
//          int num_cmds, how many commands there are
//  Purpose:
//          - executes the command or series of commands (piping)
//
int exec_commands(struct Command **cmd, int num_cmds)
{
    int fd[2 * num_cmds];

    // sets up pipe file descriptors
    for(int i = 0; i < num_cmds; i++)
        pipe(fd + 2*i);

    for(int i = 0; i < num_cmds; i++)
    {
        // executes built-in command if cmd is 'exit' 'pwd' or 'cd'
        if(is_builtin(cmd[i]->prog_name)) {
            cmd[i]->status = exec_builtin(cmd[i]);
            cmd[i]->pid = getpid();
        } else {
            cmd[i]->pid = fork();

            if (cmd[i]->pid == 0) { // child process
                change_fds(cmd, i, fd, num_cmds);
                close_fds(fd, num_cmds);
                execvp(cmd[i]->prog_name, cmd[i]->tok_args);
                fprintf(stderr, "Error: command not found \n"); 
                exit(EXIT_FAILURE);
            }
        }
    }

    // parent process
    close_fds(fd, num_cmds);

    for(int i = 0; i < num_cmds; i++) {
        if(cmd[i]->pid != getpid()) {
            // parent waits for child to finish by default
            waitpid(cmd[i]->pid, &cmd[i]->status, 0);
            cmd[i]->status = WEXITSTATUS(cmd[i]->status);
        }
    }
    
    print_completion_msg(cmd, num_cmds);
    return EXIT_SUCCESS;
}


//   
//  Purpose: 
//      - modifies file descriptor table if piping
//      - sets input/output streams for first and last command
//
void change_fds(struct Command **cmd, int i, int *fd, int num_cmds)
{
    if(i != 0)
        dup2(fd[2*(i-1)], STDIN_FILENO);

    if(i != num_cmds-1)
        dup2(fd[2*i+1], STDOUT_FILENO);

    // changes first program's stdin to the input
    if(i == 0 && cmd[i]->input) {
        int fd = open(cmd[i]->input, O_RDONLY, 0444);
        dup2(fd, STDIN_FILENO);
        close(fd);
    }
    
    // changes last program's stdout to the output
    if (i == num_cmds-1 && cmd[i]->output) {
        int fd = open(cmd[i]->output, O_WRONLY | O_CREAT, 0644);
        dup2(fd, STDOUT_FILENO);
        close(fd);
    }
}


//  Input: 
//          char *prog_name, name of program that is currently being executed
//  Purpose:
//          returns true if prog_name is a built-in command
//          otherwise returns false
//
bool is_builtin(char *prog_name)
{
    if (!strcmp(prog_name, "exit"))
        return true;
    else if (!strcmp(prog_name, "pwd"))
        return true;
    else if (!strcmp(prog_name, "cd"))
        return true;
    else
        return false;
} 

//
//  Purpose:
//          detects which built-in command to execute and executes it
//          makes sure that shell does not exit unintentionally
//          
int exec_builtin(struct Command *cmd)
{
    if (!strcmp(cmd->prog_name, "exit")) { // built in command is exit
        
        // if background process is running, report error
        if(!waitpid(-1, NULL, WNOHANG)) {
            fprintf(stderr, "Error: active jobs still running\n");
            return 1;
        }

        fprintf(stderr, "Bye...\n");
        exit(EXIT_SUCCESS);
    } else if (!strcmp(cmd->prog_name, "pwd")) { // built in command is pwd
        int dir_len = 512;
        char dir[dir_len];
        getcwd(dir, dir_len);
        fprintf(stdout, "%s\n", dir); // prints directory
        return 0;
    } else {
        int status = chdir(cmd->tok_args[1]);
        
        // changing directories fails
        if (status == -1 || cmd->num_args < 2) {
            fprintf(stderr, "Error: no such directory\n");
            return 1;
        }

        return 0;
    }
}

//
//  Purpose:
//          - closes all file descriptors created with pipe()
//
void close_fds(int *fd, int num_cmds)
{
    for(int i = 0; i < 2*num_cmds; i++)
        close(fd[i]);
}

//
//  Purpose:
//          - prints message saying that process was complete
//     
void print_completion_msg(struct Command **cmd, int num_cmds)
{
    fprintf(stderr, "+ completed '%s' ", cmd[0]->cmd_str);

    for (int i = 0; i < num_cmds; i++)
        fprintf(stderr, "[%d]" , cmd[i]->status);

    fprintf(stderr, "\n");
}