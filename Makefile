all: sshell

clean:
	rm -f sshell cmd_parser.o cmd_executor.o cmd_validator.o cmd_obj.o

sshell: sshell.c cmd_parser.o cmd_executor.o cmd_validator.o cmd_obj.o
	gcc -Wall -Werror sshell.c cmd_parser.o cmd_executor.o cmd_validator.o cmd_obj.o -o sshell

cmd_obj.o: cmd_obj.c cmd_obj.h
	gcc -Wall -Werror -c cmd_obj.c

cmd_executor.o: cmd_executor.c cmd_executor.h cmd_obj.o
	gcc -Wall -Werror -c cmd_executor.c

cmd_validator.o: cmd_validator.c cmd_validator.h cmd_parser.o cmd_obj.o
	gcc -Wall -Werror -c cmd_validator.c

cmd_parser.o: cmd_parser.c cmd_parser.h
	gcc -Wall -Werror -c cmd_parser.c
